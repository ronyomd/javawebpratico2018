var ironman = {
  title: "Iron Man",
  releasedDate: "15/05/2008",
  budget: 180000,
  poster:"http://br.web.img2.acsta.net/c_215_290/medias/nmedia/18/91/79/19/20163665.jpg"
};

var thor = {
  title: "Thor",
  releasedDate: "11/04/2011",
  budget: 170000,
  poster:"https://image.tmdb.org/t/p/w185_and_h278_bestv2/bIuOWTtyFPjsFDevqvF3QrD1aun.jpg"
};

var captainAmerica = {
  title: "Captain America",
  releasedDate: "11/07/2011",
  budget: 195000,
  poster:"https://image.tmdb.org/t/p/w185_and_h278_bestv2/vSNxAJTlD0r02V9sPYpOjqDZXUK.jpg"
};


var tbody = document.getElementById("tbodyMovies");
var btnSave = document.getElementById("btnSave");
var btnReset = document.getElementById("btnReset");
var txfTitle = document.getElementById("txfTitle");
var txfBudget = document.getElementById("txfBudget");
var txfPoster = document.getElementById("txfPoster");
var txfId = document.getElementById("txfId");
var txfReleasedDate = document.getElementById("txfReleasedDate");
var movies = [];


function init() {
  localStorage.removeItem("movies");
  addEventListeners();
  loadMovies();
}

var loadMovies = () => {
  tbody.innerHTML = "";
  movies = localStorage.getItem("movies");
  movies = JSON.parse(movies);
  var marvel = [ironman, thor, captainAmerica];
  movies = movies? movies: marvel;
  movies.forEach((m, i) => addMovieToTable(m, i));
}

var addEventListeners = () => {
  btnSave.addEventListener("click", getAndSaveMovie);

}
var addMovieToTable = function(movie, index) {
  if (!movie) return;
  var row =
          "<tr>" +
            "<td>"+movie.title+"</td>"+
            "<td>"+movie.releasedDate+"</td>"+
            "<td>"+movie.budget+"</td>"+
            "<td><img src=\""+movie.poster+"\" width=\"30px\"></td>" +
            "<td>" +
            "  <input type=\"button\" value=\"apagar\" onclick=\"removeMovie("+index+")\"/>" +
            "  <input type=\"button\" value=\"editar\" onclick=\"editMovie("+index+")\"/>" +
            "</td>" +
          "</tr>";
  tbody.innerHTML += row;
}

var removeMovie = (index) => {
    console.log("removendo filme "+movies[index].title);
    delete movies[index];
    localStorage.setItem("movies", JSON.stringify(movies));
    loadMovies();
};

var editMovie = (index) => {
    console.log("editando filme "+movies[index].title);
    var movie = movies[index];
    txfTitle.value = movie.title;
    txfPoster.value = movie.poster;
    txfReleasedDate.value = movie.releasedDate;
    txfBudget.value = movie.budget;
    txfId.value = index;
};

var getAndSaveMovie = function(e) {
  if (e) e.preventDefault();
  var title = txfTitle.value;
  var poster = txfPoster.value;
  var releasedDate = txfReleasedDate.value;
  var budget = txfBudget.value;
  var id = txfId.value;
  var isNew = id == "-1";
  id = isNew? movies.length: id;
  var movie = {id: id, title: title, "poster": poster, releasedDate: releasedDate, "budget": budget};
  if (isNew){
    movies.push(movie);
  } else {
    movies[id] = movie;
  }
  localStorage.setItem("movies", JSON.stringify(movies));
  loadMovies();
}

window.onload = init();